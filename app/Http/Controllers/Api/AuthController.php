<?php

namespace App\Http\Controllers\Api;

use App\Models\PasswordReset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Token;
use Illuminate\Support\Facades\Lang;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use http\Exception;
use Mail;


/**
 * @SWG\SecurityScheme(
 *   securityDefinition="Authorization",
 *   type="apiKey",
 *   in="header",
 *   name="Authorization"
 * )
 */
class AuthController extends Controller
{

    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        $this->apiErrorCodes = Lang::get('apiErrorCode');
    }


    /**
     * @SWG\Post(
     *     path="/auth/login",
     *     summary="Login User",
     *     tags= {"Auth"},
     *     @SWG\Parameter(
     *         name="email",
     *         in="query",
     *         required=false,
     *         description="Email: xx@gmail.com",
     *         default = "nam@gmail.com",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="email",
     *         in="query",
     *         required=false,
     *         description="Email: xx@gmail.com",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="phone",
     *         in="query",
     *         required=false,
     *         description="Zip code + phone",
     *         default = "+84942213654",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *         in="query",
     *         required=false,
     *         description="Password",
     *         default = "123456",
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Expected response to a valid request",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *     )
     * )
     */
    /**
     * @param Request $request
     * @return \App\Http\Controllers\Response|\Illuminate\Http\JsonResponse|mixed
     */
    public function login(Request $request)
    {
        try {
            $validator = Validator::make(
                $request->all(),
                [
//                    'account' => 'required|email|regex:/([a-zA-Z]{2,}[0-9]?[@][a-z]{2,}(.com)|\+([0-9]{2,3})([0-9]{9,10})/',
                    'email' => 'required_without:phone|email|regex:/([a-zA-Z]{2,}[0-9]?[@][a-z]{2,}(.com))/',
                    'phone' => 'required_without:email|regex:/\+([0-9]{2,3})([0-9]{9,10})/',
                    'password' => 'required|string|max:50',
                ],
                [
                    'email.required_without' => 50111,
                    'email.email' => 5016,
                    'email.regex' => 5017,

                    'phone.regex' => 50977,
                    'phone.required_without' => 50911,

                    'password.required' => 5021,
                    'password.string' => 5022,
                    'password.max' => 5025,
                ]);

            $arr_err = $validator->errors();


            if (count($arr_err) != 0) {

                $arr = array();
                foreach ($arr_err->all() as $error_code) {
                    array_push($arr,
                        ["errorCode" => $error_code,
                            "errorMessage" => $this->apiErrorCodes[$this->apiErrorCodes['ApiErrorCodesFlip'][$error_code]]
                        ]);
                }

                $response = array(
                    'error' => true,
                    'data' => null,
                    'errors' => $arr
                );
                return response()->json($response);

            } else {

                $email = $request['email'];
                $zip_phone = $request['phone'];
                $pass = $request['password'];

                if ($email != null) {
                    $user_email = User::where('email', $email)->first();
                    if ($email != null) {
                        if (password_verify($pass, $user_email->password)) {
                            $now = now();
                            $days = 2;
                            $expire_at = date_add($now, date_interval_create_from_date_string($days . 'days'));
                            $token = str_random(128);
                            Token::create(
                                ['user_id' => $user_email->id, 'token' => $token, '$expire_at' => $expire_at]
                            );
                            $data = DB::table('tokens')->where('tokens.token', $token)
                                ->join('users', 'tokens.user_id', '=', 'users.id')
                                ->select('users.*', 'tokens.token as token')->orderBy('created_at', 'desc')
                                ->get();
                            unset($data[0]->password);
                            return $this->respondWithSuccess($data);
                        } else {
                            $err_mess = 'wrong_account';
                        }
                    } else {
                        $err_mess = 'no_user';
                    }
                } else {
//                    return $zip_phone;
                    $user = User::all();
                    $check = false;
                    for ($i = 0; $i < count($user); $i++) {
                        $phone = $user[$i]->phone;
                        $phone = substr($phone, 1, strlen($phone));
                        $zip = $user[$i]->zipcode;
                        $zip_phone_db = $zip . $phone;
                        if ($zip_phone == $zip_phone_db) {
                            if (password_verify($pass, $user[$i]->password)) {
                                $now = now();
                                $days = 2;
                                $expire_at = date_add($now, date_interval_create_from_date_string($days . 'days'));
                                $token = str_random(128);
                                Token::create(
                                    ['user_id' => $user[$i]->id, 'token' => $token, '$expire_at' => $expire_at]
                                );
                                $data = DB::table('tokens')->where('tokens.token', $token)
                                    ->join('users', 'tokens.user_id', '=', 'users.id')
                                    ->select('users.*', 'tokens.token as token')->orderBy('created_at', 'desc')
                                    ->get();
                                unset($data[0]->password);
                                return $this->respondWithSuccess($data);
                            } else {
                                $err_mess = 'wrong_account';
                            }
                        } else {
                            $check = false;
                        }
                    }
                    if ($check == false) {
                        $err_mess = 'phone_no_exist';
                    }
                }
                $response = array(
                    "error" => true,
                    "data" => null,
                    "errors" => array([
                        'errorCode' => $this->apiErrorCodes['ApiErrorCodes'][$err_mess],
                        'errorMessage' => $this->apiErrorCodes[$err_mess]
                    ])
                );
                return response()->json($response, 400, array());
            }
        } catch (Exception $ex) {
            return $this->errorInternalError();
        };
    }


    /**
     * @SWG\Post(
     *   path="/auth/logout",
     *   summary="Log out User",
     *   tags= {"Auth"},
     *   @SWG\Response(
     *     response=200,
     *     description="Log out success"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *      security={
     *       {"Authorization": {}}
     *     }
     * )
     */
    /**
     * @param Request $request
     * @return \App\Http\Controllers\Response|mixed
     */
    public function logout(Request $request)
    {
        try {
            $token_user = $request->headers->get('Authorization');
            $token = Token::where('token', $token_user)->first();
            if ($token) {
                $token->delete();
                return $this->respondWithSuccess(null);
            } else {
                $response = array(
                    "error" => true,
                    "data" => null,
                    "errors" => array([
                        'errorCode' => $this->apiErrorCodes['ApiErrorCodes']['no_login'],
                        'errorMessage' => $this->apiErrorCodes['no_login']
                    ])
                );
                return response()->json($response, 400, array());
            }
        } catch (Exception $ex) {
            return $this->errorInternalError();
        }
    }


    /**
     * @SWG\Post(
     *     path="/auth/register",
     *     summary="Register",
     *     tags= {"Auth"},
     *     @SWG\Parameter(
     *         name="name",
     *         in="query",
     *         required=false,
     *         description="Name",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="email",
     *         in="query",
     *         required=false,
     *         description="Email",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *         in="query",
     *         required=false,
     *         description="Password",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="confirm_password",
     *         in="query",
     *         required=false,
     *         description="Confirm Password",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="dob",
     *         in="query",
     *         required=false,
     *         description="Date of birth",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="gender",
     *         in="query",
     *         required=false,
     *         description="Gender",
     *         type="boolean"
     *     ),
     *     @SWG\Parameter(
     *         name="lat",
     *         in="query",
     *         required=false,
     *         description="Latitude",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="long",
     *         in="query",
     *         required=false,
     *         description="Longitude",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="zipcode",
     *         in="query",
     *         required=false,
     *         description="Zip Code",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="phone",
     *         in="query",
     *         required=false,
     *         description="Phone Number",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="job",
     *         in="query",
     *         required=false,
     *         description="Job",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Expected response to a valid request",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *   )
     * )
     */
    /**
     * @param Request $request
     * @return \App\Http\Controllers\Response|mixed
     */
    public function register(Request $request)
    {
        try {
            $validator = Validator::make(
                $request->all(),
                [
                    'name' => 'required|string|min:3|max:50',
                    'email' => 'required|email|regex:/([a-zA-Z]{2,}[0-9]?[@][a-z]{2,}(.com))/',
                    'password' => 'required|string|max:50',
                    'confirm_password' => 'required|required_with:password|same:password',
                    'dob' => 'required|regex:/[0-9]{2}\/[0-9]{2}\/[0-9]{4}/',
                    'lat' => 'required|regex:/[0-9]{2,3}\.[0-9]{6}/',
                    'long' => 'required|regex:/[0-9]{2,3}\.[0-9]{6}/',
                    'zipcode' => 'required|string|regex:/\+[0-9]{2,3}/',
                    'phone' => 'required|string|regex:/[0-9]{10,11}/',
                    'job' => 'string',
                ],
                [
                    'name.required' => 5001,
                    'name.string' => 5002,
                    'name.min' => 5004,
                    'name.max' => 5005,
                    'email.required' => 5011,
                    'email.email' => 5016,
                    'email.regex' => 5017,
                    'password.required' => 5021,
                    'password.string' => 5022,
                    'password.max' => 5025,
                    'password.confirmed' => 5028,
                    'confirm_password.required' => 5031,
                    'confirm_password.confirmed' => 5038,
                    'password_confirmation.required_with' => 50391,
                    'password_confirmation.same' => 5039,
                    'dob.required' => 5051,
                    'dob.regex' => 5057,
                    'lat.required' => 5061,
                    'lat.regex' => 5067,
                    'long.required' => 5071,
                    'long.regex' => 5077,

                    'zipcode.required' => 5081,
                    'zipcode.regex' => 5087,
                    'phone.required' => 5091,
                    'phone.regex' => 5097,
                    'job.string' => 5102,
                ]);

            $arr_err = $validator->errors();


            if (count($arr_err) != 0) {

                $arr = array();
                foreach ($arr_err->all() as $error_code) {
                    array_push($arr,
                        ["errorCode" => $error_code,
                            "errorMessage" => $this->apiErrorCodes[$this->apiErrorCodes['ApiErrorCodesFlip'][$error_code]]
                        ]);
                }

                $response = array(
                    'error' => true,
                    'data' => null,
                    'errors' => $arr
                );
                return response()->json($response);

            } else {
                $dob_req = date_create($request['dob']);
                $dob = date_format($dob_req, 'Y/m/d H:i:s');
                if ($request['gender'] == true) {
                    $gender = 1;
                } elseif ($request['gender'] == false) {
                    $gender = 0;
                }

                $email = User::where('email', $request['email'])->first();
                $phone = User::where('phone', $request['phone'])->first();

                if ($email) {
                    $err_mess = "That Email is taken. Try another!";
                } elseif ($phone) {
                    $err_mess = 'That Phone number is taken. Try another!';
                } else {
                    $user = User::create([
                        'email' => $request['email'],
                        'password' => bcrypt($request['password']),
                        'name' => $request['name'],
                        'dob' => $dob,
                        'gender' => $gender,
                        'lat' => $request['lat'],
                        'long' => $request['long'],
                        'zipcode' => $request['zipcode'],
                        'phone' => $request['phone'],
                        'job' => $request['job'],
                    ]);
                    return $this->respondWithSuccess($user);
                }
                $response = array(
                    'error' => true,
                    'data' => null,
                    'errors' => $err_mess
                );
                return response()->json($response);
            }
        } catch (Exception $ex) {
            return $this->errorInternalError();
        }

    }



    /**
     * @SWG\Get(
     *     path="/auth/requestresetpass",
     *     summary="Request reset password",
     *     tags= {"Auth"},
     *     @SWG\Parameter(
     *         name="email",
     *         in="query",
     *         required=false,
     *         description="Email of User",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Expected response to a valid request",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *   ),
     * )
     */
    /**
     * @param Request $request
     * @return string
     */
    public function resetPassword(Request $request)
    {
        try {
            // Validate filed email
            $validator = Validator::make(
                $request->all(),
                [
                    'email' => 'required|email|regex:/([a-zA-Z]{2,}[0-9]?[@][a-z]{2,}(.com))/',
                ],
                [
                    'email.required' => 5011,
                    'email.email' => 5016,
                    'email.regex' => 5017,
                ]);

            $arr_err = $validator->errors();

            if (count($arr_err) != 0) {
                $arr = array();
                foreach ($arr_err->all() as $error_code) {
                    array_push($arr,
                        ["errorCode" => $error_code,
                            "errorMessage" => $this->apiErrorCodes[$this->apiErrorCodes['ApiErrorCodesFlip'][$error_code]]
                        ]);
                }

                $response = array(
                    'error' => true,
                    'data' => null,
                    'errors' => $arr
                );
                return response()->json($response);
            } else {
                $toEmail = $request['email'];

                $user = User::where('email', $toEmail)->first();

                if ($user) {
                    $now = now();
                    $minutes = 30;
                    $expire_at = date_add($now, date_interval_create_from_date_string($minutes . 'minutes'));
                    $code = str_random(128);
                    PasswordReset::create(
                        ['user_id' => $user->id, 'code' => $code, 'expire_at' => $expire_at]
                    );
                } else {
                    $response = array(
                        "error" => true,
                        "data" => null,
                        "errors" => array([
                            'errorCode' => $this->apiErrorCodes['ApiErrorCodes']['email_no_exist'],
                            'errorMessage' => $this->apiErrorCodes['email_no_exist']
                        ])
                    );
                    return response()->json($response, 400, array());
                }

                $data = array(
                    'url' => "http://localhost/ex-api/public/api/auth/resetpass/" . $code,
                );


//        Mail::send( "view", $data, function ($message) use ($toEmail){
                Mail::send('view-mail', $data, function ($message) use ($toEmail) {

                    $message->from('service.zalo@gmail.com', 'Zalo\'s Service');

                    $message->to($toEmail)->subject('Reset Password for Your Zalo\'s account');

                });

                return $this->respondWithSuccess('Your email has been sent successfully');
            }
        } catch (\Exception $ex) {
            return $this->errorInternalError();
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function updatePassword(Request $request)
    {
        try {
            $validator = Validator::make(
                $request->all(),
                [
                    'password' => 'required|string|max:50',
                    'password_confirmation' => 'required|required_with:password|same:password',
                ],
                [
                    'password.required' => 5021,
                    'password.string' => 5022,
                    'password.max' => 5025,
                    'password_confirmation.required' => 5031,
                    'password_confirmation.confirmed' => 5038,
                    'password_confirmation.required_with' => 50391,
                    'password_confirmation.same' => 5039,
                ]);

            $arr_err = $validator->errors();

            if (count($arr_err) != 0) {

                $arr = array();
                foreach ($arr_err->all() as $error_code) {
                    array_push($arr,
                        ["errorCode" => $error_code,
                            "errorMessage" => $this->apiErrorCodes[$this->apiErrorCodes['ApiErrorCodesFlip'][$error_code]]
                        ]);
                }

                $response = array(
                    'error' => true,
                    'data' => null,
                    'errors' => $arr
                );
                return response()->json($response);

            } else {

                $code = $request['code'];
                $pass = $request['password'];
                $pass_reset = PasswordReset::where('code', $code)->first();
                if ($pass_reset && $pass_reset->expire_at >= now()) {
                    $id = $pass_reset->user_id;
                    $user = User::find($id);
                    $user->password = bcrypt($pass);
                    $user->save();
                    return $this->respondWithSuccess('Your password has been updated');
                } else {
                    $response = array(
                        "error" => true,
                        "data" => null,
                        "errors" => array([
                            'errorCode' => $this->apiErrorCodes['ApiErrorCodes']['unavailable_page'],
                            'errorMessage' => $this->apiErrorCodes['unavailable_page']
                        ])
                    );
                    return response()->json($response, 400, array());
                }
            }
        } catch (Exception $ex) {
            return $this->errorInternalError();
        }
    }


}