<?php

namespace App\Http\Controllers\Api;

use App\Models\RequestFriend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Post;
use App\Models\Token;
use App\Models\PasswordReset;
use App\Models\Relationship;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\DB;
use Faker\Factory;

/**
 * @SWG\Swagger(
 *   basePath="/ex-api/public/api",
 *   @SWG\Info(
 *     title="User API",
 *     version="2.x"
 *   )
 * )
 */
class UserController extends Controller
{
    /**
     * @var $user_id
     */
    public $user_id;


    /**
     * UserController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        GLOBAL $user_id;
        $this->apiErrorCodes = Lang::get('apiErrorCode');

        $token = $request->headers->get('Authorization');
        try {
            if ($token != null) {
                $info_token = Token::where('token', $token)->first();
                $user_id = $info_token->user_id;
            } else {
                return $this->errorInternalError();
            }
        } catch (\Exception $ex) {
            return $this->errorInternalError();
        }

    }




    /**
     * @SWG\Get(
     *   path="/admin/users",
     *   summary="List users",
     *   tags= {"Admin"},
     * @SWG\Response(
     *     response=200,
     *     description="A list with users"
     *   ),
     * @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *      security={
     *       {"Authorization": {}}
     *     }
     *
     * )
     */
    /**
     * @return \App\Http\Controllers\Response|mixed
     */
    public function index()
    {
        try {
            $users = User::all();
            return $this->respondWithSuccess($users);
        } catch (\Exception $ex) {
            return $this->errorInternalError();
        }
    }


    /**
     * @SWG\Get(
     *     path="/users",
     *     summary="Get User Info",
     *     tags= {"User"},
     *     @SWG\Response(
     *         response=200,
     *         description="Expected response to a valid request",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *   ),
     *      security={
     *       {"Authorization": {}}
     *     }
     * )
     */
    /**
     * @return \App\Http\Controllers\Response|mixed
     */
    public function showUser()
    {
        try {
            GLOBAL $user_id;
            return $this->respondWithSuccess(User::find($user_id));
        } catch (\Exception $ex) {
            return $this->errorInternalError();
        }
    }


    /**
     * @SWG\Put(
     *     path="/users",
     *     summary="Update User",
     *     tags= {"User"},
     *     @SWG\Parameter(
     *         name="email",
     *         in="query",
     *         required=true,
     *         description="Email",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *         in="query",
     *         required=true,
     *         description="Password",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="query",
     *         required=true,
     *         description="Name",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="phone",
     *         in="query",
     *         required=true,
     *         description="Phone",
     *         type="number"
     *     ),
     *     @SWG\Parameter(
     *         name="gender",
     *         in="query",
     *         required=true,
     *         description="Gender",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="age",
     *         in="query",
     *         required=true,
     *         description="Age",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="address",
     *         in="query",
     *         required=true,
     *         description="Address",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="job",
     *         in="query",
     *         required=true,
     *         description="Job",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Expected response to a valid request",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *   ),
     *      security={
     *       {"Authorization": {}}
     *     }
     * )
     */
    /**
     * @param Request $request
     * @return \App\Http\Controllers\Response|mixed
     */
    public function updateUser(Request $request)
    {
        try {
            GLOBAL $user_id;
            $user = User::find($user_id);
            $user->update([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
                'phone' => $request['phone'],
                'address' => $request['address'],
                'job' => $request['job'],
                'age' => $request['age'],
                'gender' => $request['gender'],
            ]);
            return $this->respondWithSuccess($user);
        } catch (\Exception $ex) {
            return $this->errorInternalError();
        }

    }



    /**
     * @SWG\Delete(
     *     path="/admin/users/{id}",
     *     summary="Delete User by ID",
     *     tags= {"Admin"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="The id of the user to retrieve",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Expected response to a valid request",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *   ),
     *      security={
     *       {"Authorization": {}}
     *     }
     * )
     */
    /**
     * @param $id
     * @return \App\Http\Controllers\Response|mixed
     */
    public function deleteUser($id)
    {
        try {
            $user = User::find($id);
            $email = $user->email;
            $user->delete();
            return $this->respondWithSuccess($email);
        } catch (\Exception $ex) {
            return $this->errorInternalError();
        }

    }


    /**
     * @SWG\Get(
     *     path="/users/posts",
     *     summary="Get Posts of User",
     *     tags= {"User"},
     *     @SWG\Response(
     *         response=200,
     *         description="Expected response to a valid request",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *   ),
     *      security={
     *       {"Authorization": {}}
     *     }
     * )
     */
    /**
     * @return \App\Http\Controllers\Response|mixed
     */
    public function getPosts()
    {
        try {
            GLOBAL $user_id;
            $posts = Post::where('user_id', $user_id)->get();
            return $this->respondWithSuccess($posts);
        } catch (\Exception $ex) {
            return $this->errorInternalError();
        }

    }


    /**
     * @SWG\Post(
     *     path="/users/posts",
     *     summary="Add Post",
     *     tags= {"User"},
     *     @SWG\Parameter(
     *         name="article",
     *         in="query",
     *         required=true,
     *         description="content of the Post",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Expected response to a valid request",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *   ),
     *      security={
     *       {"Authorization": {}}
     *     }
     * )
     */
    /**
     * @param Request $request
     * @return \App\Http\Controllers\Response|mixed
     */
    public function addPost(Request $request)
    {
        try {
            GLOBAL $user_id;
            $addPost = Post::create([
                'user_id' => $user_id,
                'article' => $request['article'],
            ]);
            return $this->respondWithSuccess($addPost);
        } catch (\Exception $ex) {
            return $this->errorInternalError();
        }

    }


    /**
     * @SWG\Put(
     *     path="/users/posts/{post_id}",
     *     summary="Update Post OF USER",
     *     tags= {"User"},
     *     @SWG\Parameter(
     *         name="post_id",
     *         in="path",
     *         required=true,
     *         description="Post id",
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="article",
     *         in="query",
     *         required=true,
     *         description="content of the Post",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Expected response to a valid request",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *   ),
     *      security={
     *       {"Authorization": {}}
     *     }
     * )
     */
    /**
     * @param Request $request
     * @param $post_id
     * @return \App\Http\Controllers\Response|\Illuminate\Http\JsonResponse|mixed
     */
    public function updPost(Request $request, $post_id)
    {
        try {
            GLOBAL $user_id;
            $post = Post::where('user_id', $user_id)->where('id', $post_id)->first();
            if ($post != null) {
                $post->update([
                    'article' => $request['article'],
                ]);
                return $this->respondWithSuccess($post);
            } else {
                $response = array(
                    "error" => true,
                    "data" => null,
                    "errors" => array(
                        'errorCode' => $this->apiErrorCodes['ApiErrorCodes']['no_post_of_user'],
                        'errorMessage' => $this->apiErrorCodes['no_post_of_user'],
                    )
                );
                return response()->json($response, 400, array());
            }

        } catch (\Exception $ex) {
            return $this->errorInternalError();
        }
    }


    /**
     * @SWG\Delete(
     *     path="/users/posts/{post_id}",
     *     summary="Delete Post OF USER",
     *     tags= {"User"},
     *     @SWG\Parameter(
     *         name="post_id",
     *         in="path",
     *         required=true,
     *         description="The id of the user to retrieve",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Expected response to a valid request",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *   ),
     *      security={
     *       {"Authorization": {}}
     *     }
     * )
     */
    /**
     * @param $post_id
     * @return \App\Http\Controllers\Response|\Illuminate\Http\JsonResponse|mixed
     */
    public function deletePost($post_id)
    {
        try {
            GLOBAL $user_id;
            $post = Post::where('user_id', $user_id)->where('id', $post_id)->first();
            if ($post != null) {
                $post->delete();
                return $this->respondWithSuccess(null);
            } else {
                $response = array(
                    "error" => true,
                    "data" => null,
                    "errors" => array([
                        'errorCode' => $this->apiErrorCodes['ApiErrorCodes']['no_post_of_user'],
                        'errorMessage' => $this->apiErrorCodes['no_post_of_user'],
                    ])
                );
                return response()->json($response, 400, array());
            }
        } catch (\Exception $ex) {
            return $this->errorInternalError();
        }
    }



    /**
     * @SWG\Post(
     *     path="/users/addfriend/{id}",
     *     summary="Add Friend by Friend's Id",
     *     tags= {"User"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="Id of friend",
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Expected response to a valid request",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *   ),
     *      security={
     *       {"Authorization": {}}
     *     }
     * )
     */
    /**
     * @param Request $request
     * @return \App\Http\Controllers\Response|mixed
     */
    public function addFriend($id)
    {
        try {
            GLOBAL $user_id;
            $friend_id = $id;
            $relationship = Relationship::where('user_id', $user_id)->where('friend_id', $friend_id)->first();
            if ($relationship == null) {
                $addFriend1 = Relationship::create([
                    'user_id' => $user_id,
                    'friend_id' => $friend_id,
                ]);
                $addFriend2 = Relationship::create([
                    'user_id' => $friend_id,
                    'friend_id' => $user_id,
                ]);
                return $this->respondWithSuccess([$addFriend1, $addFriend2]);
            } else {
                $response = array(
                    "error" => true,
                    "data" => null,
                    "errors" => array(
                        'errorCode' => $this->apiErrorCodes['ApiErrorCodes']['related'],
                        'errorMessage' => $this->apiErrorCodes['related'],
                    )
                );
                return response()->json($response, 400, array());
            }

        } catch (\Exception $ex) {
            return $this->errorInternalError();
        }

    }


    /**
     * @SWG\Get(
     *     path="/users/{id}/posts",
     *     summary="Get Posts of Friend",
     *     tags= {"User"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="Id of friend",
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Expected response to a valid request",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *   ),
     *      security={
     *       {"Authorization": {}}
     *     }
     * )
     */
    /**
     * @return \App\Http\Controllers\Response|mixed
     */
    public function getPostsOfFriend(Request $request)
    {
        try {
            GLOBAL $user_id;

            $relationship = Relationship::where('user_id', $user_id)->where('friend_id', $request['id'])->first();
            if ($relationship == null) {
                $response = array(
                    "error" => true,
                    "data" => null,
                    "errors" => array(
                        'errorCode' => $this->apiErrorCodes['ApiErrorCodes']['not_related'],
                        'errorMessage' => $this->apiErrorCodes['not_related'],
                    )
                );
                return response()->json($response, 400, array());
            } else {
                $posts = Post::where('user_id', $request['id'])->get();
                return $this->respondWithSuccess($posts);
            }

        } catch (\Exception $ex) {
            return $this->errorInternalError();
        }

    }


    /**
     * @SWG\Get(
     *     path="/users/newfeeds",
     *     summary="Get NewFeeds",
     *     tags= {"User"},
     *     @SWG\Response(
     *         response=200,
     *         description="Expected response to a valid request",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *   ),
     *      security={
     *       {"Authorization": {}}
     *     }
     * )
     */
    /**
     * @return \App\Http\Controllers\Response|\Illuminate\Http\JsonResponse|mixed
     */
    public function getNewFeeds()
    {
        try {
            GLOBAL $user_id;

            $friend_id = DB::table('relationships')->where('user_id', $user_id)
                ->select('friend_id')
                ->get();
            if ($friend_id == null) {
//                $posts = Post::where('user_id', $user_id)->orderBy('created_at', 'DESC')->get();
                $posts = DB::table('posts')
                    ->join('users', 'users.id', '=', 'posts.user_id')
                    ->select('users.*', 'posts.article', 'posts.created_at as created_post', 'posts.updated_at as updated_post')
                    ->where('user_id', $user_id)
                    ->orderBy('created_at', 'DESC')->paginate(2);
            } else {
                $arr_fr_id = array($user_id);
                for ($i = 0; $i < count($friend_id); $i++) {
                    $fr_id = $friend_id[$i]->friend_id;
                    array_push($arr_fr_id, $fr_id);
                }
//                $posts = Post::whereIn('user_id', $arr_fr_id)->orderBy('created_at', 'DESC')->paginate(2);
                $posts = DB::table('posts')
                    ->join('users', 'users.id', '=', 'posts.user_id')
                    ->select('users.*', 'posts.article', 'posts.created_at as created_post', 'posts.updated_at as updated_post')
                    ->whereIn('user_id', $arr_fr_id)
                    ->orderBy('created_at', 'DESC')->paginate(2);
                for ($i = 0; $i < count($posts); $i++) {
                    unset($posts[$i]->password);
                }
                unset($posts->password);
            }
            return $this->respondWithSuccess($posts);
        } catch (\Exception $ex) {
            return $this->errorInternalError();
        }
    }


    /**
     * @SWG\Get(
     *     path="/users/findfriends",
     *     summary="Find Friends",
     *     tags= {"User"},
     *     @SWG\Parameter(
     *         name="gender",
     *         in="query",
     *         required=false,
     *         description="Find by gender",
     *          type="string",
     *          enum={"Female", "Male"}
     *     ),
     *     @SWG\Parameter(
     *         name="age",
     *         in="query",
     *         required=false,
     *         description="Age about",
     *          type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Expected response to a valid request",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *   ),
     *      security={
     *       {"Authorization": {}}
     *     }
     * )
     */
    /**
     * @param Request $request
     * @return \App\Http\Controllers\Response|mixed
     */
    public function findFriends(Request $request)
    {
        try {
//            GLOBAL $user_id;
//            $arr_id = User::select('id')->get();
//            $arr_dist = array();
//            $arr_user = array();
//            for ($i = 0; $i < count($arr_id); $i++) {
//                $friend_id = $arr_id[$i]->id;
//                if ($user_id != $friend_id) {
//                    $relationship = Relationship::where('user_id', $user_id)->where('friend_id', $friend_id)->first();
//                    if ($relationship == null) {
//                        $user1 = User::where('id', $user_id)->first();
//                        $user2 = User::where('id', $friend_id)->first();
////                        $data = DB::select('SELECT ST_Distance_Sphere(
////                            POINT(' . $user1->lat . ', ' . $user1->long . '),
////                            POINT(' . $user2->lat . ', ' . $user2->long . ')
////                            ) as \'distance\'');
//                        $data = DB::SELECT('select *, ST_Distance_Sphere(
//                            POINT(' . $user1->lat . ', ' . $user1->long . '),
//                            POINT(' . $user2->lat . ', ' . $user2->long . ')
//                            ) as \'distance\' from users where id =' . $friend_id . '');
//
//                        $dist = $data[0]->distance;
//                        unset($data[0]->password);
//                        $data = (array)$data;
//
//                        if ($dist <= 500) {
//                            array_push($arr_user, $data);
//                        }
//                        if (count($arr_user) > 1)
//                            return $this->respondWithSuccess($arr_user);
//                    }
//                }
//            }
//            return $arr_user;
//            return $this->respondWithSuccess($arr_user);

            $gender = $request['gender'];
            $age = $request['age'];
            GLOBAL $user_id;
            $user = User::where('id', $user_id)->first();
//            $arr_fr_id = Relationship::select('friend_id')->where('user_id',$user_id)->get();
            if ($gender != null && $age == null) {
                if ($gender == 'Female') {
                    $gender = 1;
                } else $gender = 0;

                $users = User::select(
                    DB::raw("*, (YEAR(CURRENT_DATE())-YEAR(users.dob)) as age,
                            (6371393 * acos(cos
                            ( radians(  ?  ) ) *
                            cos( radians( users.lat ) ) * 
                            cos( radians( users.long ) - radians(?) ) +
                            sin( radians(  ?  ) ) *
                            sin( radians( users.lat ) ) 
                            )) AS distance"))
                    ->setBindings([$user->lat, $user->long, $user->lat])->where('gender', $gender)
                    ->whereNotIn('id', [$user_id])
                    ->orderBy("distance")->get();
            } else if ($gender == null && $age != null) {
                $users = User::select(
                    DB::raw("*,(YEAR(CURRENT_DATE())-YEAR(users.dob)) as age,
                           (6371393 * acos(cos
                          ( radians(  ?  ) ) *
                          cos( radians( users.lat ) ) * 
                          cos( radians( users.long ) - radians(?) ) +
                          sin( radians(  ?  ) ) *
                          sin( radians( users.lat ) ) 
                          )) AS distance"))
                    ->setBindings([$user->lat, $user->long, $user->lat])
                    ->whereNotIn('id', [2])->whereRaw('(YEAR(CURRENT_DATE())-YEAR(users.dob)) between ? and ? ', [$age - 3, $age + 3])
                    ->orderBy("distance")->get();
            } else if ($gender != null && $age != null) {
                if ($gender == 'Female') {
                    $gender = 1;
                } else $gender = 0;
                $users = User::select(
                    DB::raw("*,(YEAR(CURRENT_DATE())-YEAR(users.dob)) as age,
                           (6371393 * acos(cos
                          ( radians(  ?  ) ) *
                          cos( radians( users.lat ) ) * 
                          cos( radians( users.long ) - radians(?) ) +
                          sin( radians(  ?  ) ) *
                          sin( radians( users.lat ) ) 
                          )) AS distance"))
                    ->setBindings([$user->lat, $user->long, $user->lat])
                    ->whereNotIn('id', [2])
                    ->whereRaw('gender = ? and ((YEAR(CURRENT_DATE())-YEAR(users.dob)) between ? and ?)', [$gender, $age - 3, $age + 3])
                    ->orderBy("distance")->get();
//                $user = User::select(DB::raw('id, name,(YEAR(CURRENT_DATE())-YEAR(users.dob))as age'))->get();
            } else {
                $users = User::select(
                    DB::raw("*, (YEAR(CURRENT_DATE())-YEAR(users.dob)) as age,
                           (6371393 * acos(cos
                          ( radians(  ?  ) ) *
                          cos( radians( users.lat ) ) * 
                          cos( radians( users.long ) - radians(?) ) +
                          sin( radians(  ?  ) ) *
                          sin( radians( users.lat ) ) 
                          )) AS distance"))
                    ->setBindings([$user->lat, $user->long, $user->lat])
                    ->whereNotIn('id', [$user_id])
                    ->orderBy("distance")->get();
            }

            return $this->respondWithSuccess($users);


        } catch (\Exception $ex) {
            return $this->errorInternalError();
        }
    }


    /**
     * @SWG\Get(
     *     path="/users/sendrequest/{id}",
     *     summary="Send request Friend by id",
     *     tags= {"User"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="Id of friend",
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="message",
     *         in="query",
     *         required=true,
     *         description="Message for request",
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Expected response to a valid request",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *   ),
     *      security={
     *       {"Authorization": {}}
     *     }
     * )
     */
    /**
     * @return \App\Http\Controllers\Response|\Illuminate\Http\JsonResponse|mixed
     */
    public function sendRequestFriend(Request $request, $id)
    {
        try {

            GLOBAL $user_id;
            $friend_id = $id;
            $mess = $request['message'];
            $relationship = Relationship::where('user_id', $user_id)->where('friend_id', $friend_id)->first();


            if ($relationship == null) {
                $sendRequest = RequestFriend::create([
                    'from_id' => $user_id,
                    'to_id' => $friend_id,
                    'messages' => $mess,
                ]);
                return $this->respondWithSuccess($sendRequest);

            } else {
                $response = array(
                    "error" => true,
                    "data" => null,
                    "errors" => array(
                        'errorCode' => $this->apiErrorCodes['ApiErrorCodes']['related'],
                        'errorMessage' => $this->apiErrorCodes['related'],
                    )
                );
                return response()->json($response, 400, array());
            }
        } catch
        (\Exception $ex) {
            return $this->errorInternalError();
        }
    }


    /**
     * @SWG\Post(
     *     path="/users/friendrequests/",
     *     summary="List Friend Requests",
     *     tags= {"User"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="query",
     *         required=true,
     *         description="Id of friend Who ",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="accept",
     *         in="query",
     *         required=false,
     *         description="Find by gender",
     *          type="string",
     *          enum={"Accept", "Delete Request"}
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Expected response to a valid request",
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="unexpected error",
     *   ),
     *      security={
     *       {"Authorization": {}}
     *     }
     * )
     */
    /**
     * @return \App\Http\Controllers\Response|\Illuminate\Http\JsonResponse|mixed
     */
    public function friendRequests(Request $request)
    {
        try {

            GLOBAL $user_id;
            $friend_id = $request['id'];
            $decision = $request['accept'];
            $request_friend = RequestFriend::where('from_id', $friend_id)->where('to_id', $user_id)->first();
            $relationship = Relationship::where('user_id', $user_id)->where('friend_id', $friend_id)->first();
            if ($request_friend != null) {
                if ($decision == "Accept") {
                    $addFriend1 = Relationship::create([
                        'user_id' => $user_id,
                        'friend_id' => $friend_id,
                    ]);
                    $addFriend2 = Relationship::create([
                        'user_id' => $friend_id,
                        'friend_id' => $user_id,
                    ]);
                    RequestFriend::where('from_id', $user_id)->where('to_id', $friend_id)->delete();
                    return $this->respondWithSuccess([$addFriend1, $addFriend2]);
                } else {
                    RequestFriend::where('from_id', $user_id)->where('to_id', $friend_id)->delete();
                    return $this->respondWithSuccess("Delete Request Success!");
                }

            } else if ($relationship != null) {
                $mess_code = 'related';
            } else {
                $mess_code = 'not_request';
            }
            $response = array(
                "error" => true,
                "data" => null,
                "errors" => array(
                    'errorCode' => $this->apiErrorCodes['ApiErrorCodes'][$mess_code],
                    'errorMessage' => $this->apiErrorCodes[$mess_code],
                )
            );
            return response()->json($response, 400, array());
        } catch
        (\Exception $ex) {
            return $this->errorInternalError();
        }
    }


    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function test(Request $request, $id)
    {
        //$user = new User();
//        $pass = DB::table('users')->where('id', $id)->value('password');
//       $user = DB::table('users')->where('name', 'nam')->first();
//        echo $pass;
//        if ($pass == '123456')
//            return 'đúng pass rồi';
//        return 'sai cmn password';
//        return $user;
//        return $pass;
        //$post = DB::table('tokens')->where('id', $id)->value('token');


        // Hash, bcrypt password, verify
//        $pass1 = '123456';
//        $pass1 = Hash::make($pass1);
//        echo $pass1. '<br>';
//
//
//        $hash = '$2y$10$WuFtFSUvmqaOl6LCmV34seSsWp0TmIre6SejPM7xjGRiSybGIQahG';
//
//        if (password_verify('123456', $hash)) {
//            echo 'Password is valid!';
//        } else {
//            echo 'Wrong .';
//        }


//        $token = 'N9KxpfUORBPZqMskF7JOobzJFCQJDQNSUJ2c69VR6yO6iGgBglLCPDuxBnDoW1srDDETgKkbjA0b7vjfQhB4eY2BG4uTmDaeNs9DoAREK9jHofbsDpdD5ScSmGrgiZll';
////        $token_db = DB::table('tokens')->where('token', $token)->first();
//        $token_db = Token::where('token', $token)->first()->expire;
////        $datetime = $token_db->expired;
//        $expire = date_create($token_db);
//        echo date_format($expire, "d-m-Y H:i:s");
////        echo $datetime->toString();


//        $undefine = Post::all();
//        $undefine = User::with('Post', 'Token')
//            ->select('users.name', 'posts.article', 'tokens.token')->get();
//        $undefine = User::with('Post', 'Token')->first();
//        $undefine = Post::with('User')->first();
//        $undefine = Post::with('User')->find(5)->first();
//        $undefine->name;
//        echo $undefine;
//        echo $undefine->users.name;
//        $user=User::find(1)->token()->token->get();
//        $user=User::find(1)->first();


//        $token = Token::where('user_id', 1)->first();
//
//        echo $token->user->name;
//        $post = Post::where('id', 1)->where('user_id', 3)->first();
//        return $post;
//        $errors = [
//            'email' => ["11111", "2222"],
//            'password' =>  "4444",
//            'c' => 999999,
//            'd' => [77, 88],
//        ];


//        $errors_code = array();
//        $count_err = count($errors);
//        for ($i = 0; $i < $count_err; $i++) {
//
//            // get first item
//            $shift = array_shift($errors);
//
//            if (count($shift) == 1) {
//                $arr_temp = array($shift);
//                if ($errors_code == null)
//                    $errors_code = $arr_temp;
//                else
//                    $errors_code = array_merge($errors_code, $arr_temp);
//            } else {
//                $val = array_values($shift);
//                if ($errors_code == null)
//                    $errors_code = $val;
//                else
//                    $errors_code = array_merge($errors_code, $val);
//            }
//        }


//        $shift = array_shift($errors);
//        $a = array_values($shift);
//        $shift2 = array_shift($errors);
//        $b = array_values($shift2);
//        $shift3 = array_shift($errors);
//        $c= array_values($shift3);
//        $abc = array_merge($a,$b,$c, $bbb);

//        $a = ['az' => 11111, 'ay' => 2222];
//        $b = array_shift($a);
//        $errors = [
//            'email' => ["11111", "2222"],
//            'password' =>  "4444",
//            'c' => 999999,
//            'd' => [77, 88],
//        ];
//
//
//        $a = array($errors);
//        $b=array_shift($a);

//
//        $errors = [
//            'email' => "1001",
//            'password' => ["1005", "5555"],
//        ];

//        $count_err = count($errors);
//        for ($i = 0; $i < $count_err; $i++) {
//
//            // get first item
//            $shift = array_shift($errors);
//
//            if (count($shift) == 1) {
//                $arr_temp = array($shift);
//                if ($errors_code == null)
//                    $errors_code = $arr_temp;
//                else
//                    $errors_code = array_merge($errors_code, $arr_temp);
//            } else {
//                $val = array_values($shift);
//                if ($errors_code == null)
//                    $errors_code = $val;
//                else
//                    $errors_code = array_merge($errors_code, $val);
//            }
//        }


//        $users = DB::table('users')->where('users.id', 1)
//            ->join('tokens', 'users.id', '=', 'tokens.user_id')
//            ->select('users.*', 'tokens.token as token')
//            ->get();
//        $data = DB::table('tokens')->where('tokens.id', 1)
//            ->join('users', 'tokens.user_id', '=', 'users.id')
//            ->select('users.*', 'tokens.token as token')
//            ->get();
//        return ($data);

//        $faker = Factory::create('en_US');
//
//        $limit = 5;
//        $dob= $faker->dateTime('now');
//
//        $user = array([
//            'email' => $faker->unique()->email,
//            'password' => bcrypt('123456'),
//            'name' => $faker->name(50),
//            'dob' =>date_format($dob, "d-m-Y"),
//            'gender' => $faker->numberBetween(0,1),
//            'lat' => random_int(10000000,999999999)/1000000,
//            'long' => random_int(10000000,999999999)/1000000,
//            'phone' => random_int(1000000000,1111111111),
//            'job' => $faker->domainName,
//        ]);
//
//return $user;


//        $now =now();
//        $hours = 2;
//        $expire = date_add($now, date_interval_create_from_date_string($hours. 'hours'));
//
//        for ($i = 0; $i < 3; $i++) {
//            Token::create([
//                'user_id' => rand(1, 5),
//                'token' => 'sadfkjhsdafkjsdhfkj',
//                'expire_at' => $expire
//            ]);
////            DB::table('tokens')->insert([
////                'user_id' => rand(1,5),
////                'token' => str_random(128),
////                'expire_at' => $expire,
////            ]);
//        }

//        $id = User::where('email', 'nams@gmail.com')->first();
//        if ($id == null)
//            return 'email is worng';
//        return $id;


//         $user_id = 1;
//            $friend_id = DB::table('relationships')->where('user_id', 1)
//                ->select('friend_id')
//                ->get();
//            if ($friend_id == null) {
//                $posts = Post::where('user_id', $user_id)->orderBy('created_at', 'DESC')->get();
//                return 'no friend';
//            } else {
//                for ($i = 0; $i < count($friend_id); $i++) {
//                    $fr_id = $friend_id[$i]->friend_id;
//                    $posts = Post::where('friend_id', $fr_id)->orderBy('created_at', 'DESC')->get();
//                }
//            }


//        $posts = Post::whereIn('user_id', [1,3,5,4])->orderBy('created_at', 'DESC')->get();
//            return $posts;


//        $friend_id = DB::table('relationships')->where('user_id', 1)->get();
//
//        $arr = array();
//        for ($i = 0; $i < count($friend_id); $i++) {
//            $fr_id = $friend_id[$i]->friend_id;
//            array_push($arr, $fr_id);
//
//        }
//
//        $posts = Post::whereIn('user_id', $arr)->orderBy('created_at', 'DESC')->get();
//
//        return $posts;


//        $user_id = rand(1,10);
//        $friend_id = rand(1,10);
//        while ($user_id == $friend_id){
//            $friend_id = rand(1,10);
//        }
//
//        echo $user_id. "    ". $friend_id;


//        $dob_req = date_create($request['15/15/2001']);
//        $dob = date_format($dob_req, 'Y/m/d H:i:s');
//    return $dob;


//        GLOBAL $user_id;
//        $location = DB::table('users')->where('id',10 )
//            ->select('lat','long')
//            ->get();
//        return $location;

//        $data = DB::table('tokens')->where('tokens.user_id', 10)
//            ->join('users', 'tokens.user_id', '=', 'users.id')
//            ->select('users.*', 'tokens.token as token')
//            ->get();
////        array_slice($data[0], 2, 1);
//        unset($data[0]->password);
//
//        return $data;


//        $data = DB::table('tokens')->where('tokens.user_id', 10)
//            ->join('users', 'tokens.user_id', '=', 'users.id')
//            ->select('users.*', 'tokens.token as token', 'tokens.created_at as createeeeeee')->orderBy('tokens.created_at', 'desc')
//            ->get();
//
//        return $data;


//        $raw = DB::raw('ROUND ( ( 6371 * acos( cos( radians(' . '121.251324' . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . '132.546546' . ') ) + sin( radians(' . '546.321132' . ') ) * sin( radians( latitude ) ) ) ) ) AS distance');
//
//        return $raw;
//        return $query->select('*')->addSelect($raw)->orderBy( 'distance', 'ASC' )->groupBy('distance')->having('distance', '<=', $distance);


//        $user1 = User::where('id', 1)->first();
//        $user2 = User::where('id', 2)->first();
//        $lat1 = $user1->lat;
//        $long1 = $user1->long;
//        $lat2 = $user2->lat;
//        $long2 = $user2->long;
//        $data = DB::select('SELECT ST_Distance_Sphere(
//        POINT(' . $lat1 . ', ' . $long1 . '),
//        POINT(' . $lat2 . ', ' . $long2 . ')
//    ) as \'distance\'');
//        return $data;


//        $user1 = User::where('id', 10)->first();
//        $user2 = User::where('id', 10)->first();
//        $data = DB::select('SELECT ST_Distance_Sphere(
//                    POINT(' . $user1->lat . ', ' . $user1->long . '),
//                    POINT(' . $user2->lat . ', ' . $user2->long . ')
//                    ) as \'distance\'');
//
//        return $data[0]->distance;


//        $relationship = Relationship::where('user_id', 10)->where('friend_id', 5)->first();
//        if ($relationship == []) {
//            $user1 = User::where('id', 10)->first();
//            $user2 = User::where('id', 5)->first();
//            $data = DB::select('SELECT ST_Distance_Sphere(
//                    POINT(' . $user1->lat . ', ' . $user1->long . '),
//                    POINT(' . $user2->lat . ', ' . $user2->long . ')
//                    ) as \'distance\'');
//
//            $arr = $data[0]->distance;
//            return $arr;
//        }
//                else {
//                    array_push($arr, $data[0]->distance);
//                }


//        $id = User::select('id')->get();
//        $arr = array();
//        for ($i = 1; $i < count($id); $i++) {
//            $relationship = Relationship::where('user_id', 10)->where('friend_id', $id[$i]->id)->first();
//
//            if ($relationship == null) {
//                $user1 = User::where('id', 10)->first();
//                $user2 = User::where('id', $id[$i]->id)->first();
//                $data = DB::select('SELECT ST_Distance_Sphere(
//                    POINT(' . $user1->lat . ', ' . $user1->long . '),
//                    POINT(' . $user2->lat . ', ' . $user2->long . ')
//                    ) as \'distance\'');
//                $dist = $data[0]->distance;
//                array_push($arr, $dist);
//            }
//        }
//        return $arr;


//        $user_id = 7;
//        $arr_id = User::select('id')->get();
//
//        $arr = array();
//        for ($i = 0; $i < count($arr_id); $i++) {
//            $friend_id = $arr_id[$i]->id;
//            if ($user_id != $friend_id){
//                $relationship = Relationship::where('user_id', $user_id)->where('friend_id', $friend_id)->first();
//                if ($relationship == null) {
//                    $user1 = User::where('id', 10)->first();
//                    $user2 = User::where('id', $friend_id)->first();
//                    $data = DB::select('SELECT ST_Distance_Sphere(
//                    POINT(' . $user1->lat . ', ' . $user1->long . '),
//                    POINT(' . $user2->lat . ', ' . $user2->long . ')
//                    ) as \'distance\'');
//                    $dist = $data[0]->distance;
//                    array_push($arr, $dist);
//                }
//            }
//        }
//return $arr;


//        $data = DB::select('SELECT ST_Distance_Sphere(
//                    POINT(10.823095, 106.629665),
//                    POINT(10.823097, 106.629661)
//                    ) as \'distance\'');
//        return $data;


//        $user_id = 2;
//        $arr_id = User::select('id')->get();
//        $arr_dist = array();
//        $arr_user_id = array();
//        for ($i = 0; $i < count($arr_id); $i++) {
//            $friend_id = $arr_id[$i]->id;
//            if ($user_id != $friend_id){
//                $relationship = Relationship::where('user_id', $user_id)->where('friend_id', $friend_id)->first();
//                if ($relationship == null) {
//                    $user1 = User::where('id', $user_id)->first();
//                    $user2 = User::where('id', $friend_id)->first();
//                    $data = DB::select('SELECT ST_Distance_Sphere(
//                            POINT(' . $user1->lat . ', ' . $user1->long . '),
//                            POINT(' . $user2->lat . ', ' . $user2->long . ')
//                            ) as \'distance\',');
//                    if ($data[0]->distance <= 500) {
//                        $data = (array) $data[0];
////                        return $data;
//                        array_push($arr_dist, $data);
//                        array_push($arr_user_id, $user2);
//                    }
//                }
//            }
//        }
////        $arr_user_id = (array) $arr_user_id;
//        $temp = (array) $arr_user_id;
//        for ($i = 0; $i<count($arr_user_id);$i++){
////            $temp = (array) $arr_user_id;
////            return $temp;
//            array_push($temp[$i],$arr_dist[$i]);
//        }
////        array_push($arr_user_id[0],$arr_dist[0]);
////        var_dump( (array) $arr_user_id );
//    return $temp;


////        return User::all();
//        $a = DB::SELECT('select *, ST_Distance_Sphere(
//                    POINT(10.823095, 106.629665),
//                    POINT(10.823097, 106.629661)
//                    ) as \'distance\' from users where users.id = 2');
//
//        $b = DB::SELECT('select *, ST_Distance_Sphere(
//                    POINT(10.823095, 106.629665),
//                    POINT(10.823097, 106.629661)
//                    ) as \'distance\' from users where users.id = 3');
//
//        $c = DB::SELECT('select *, ST_Distance_Sphere(
//                    POINT(10.823095, 106.629665),
//                    POINT(10.823097, 106.629661)
//                    ) as \'distance\' from users where users.id = 4');
//        $a = (array) $b;
//        array_push($a,$b);
//        array_push($a,$b);
////        array_push($a,$c);
//
//        return $a;


//        return DB::table('users')->where('users.id', 2)
//            ->select('users.*, SELECT ST_Distance_Sphere(
//                    POINT(10.823095, 106.629665),
//                    POINT(10.823097, 106.629661)
//                    ) as \'distance\'')->get();


//       return  DB::table('users')->where('users.id', 2)
//            ->select('users.*')->get();

//        $users = DB::table('users')->paginate(5);
//        return $this->respondWithSuccess($users);


//        $posts = Post::whereIn('user_id', [1,2,3,4])->orderBy('created_at', 'DESC')->paginate(3);
//        return $posts;


//        $zip_phone = '+9012345789';
//        $user = User::all();
//        for ($i = 0; $i < count($user); $i++) {
//            $phone = $user[$i]->phone;
//            $phone = substr($phone, 1, strlen($phone));
//            $zip = $user[$i]->zipcode;
//            $zip_phone_db = $zip . $phone;
//            if ($zip_phone == $zip_phone_db)
//                return "Login success";
//            else return "login fail";
//        }

//        $data = DB::unprepared('select *, ST_Distance_Sphere(
//                            POINT(10.121212, 106.101010),
//                            POINT(10.121200, 106.101011)
//                            ) as distance from users where id =2');


//        $user = User::where('id', 2)->first();
//        $dob = date_create(now());
//
//        $temp = date_format($dob,"Y");
//        $dob = $temp - 22;
//
//        $temp = date_format($dob,"Y-m-d");
//        return $temp;
//
////        $ids = $user->id;
////        $lat = $user->lat;
////        $long = $user->long;
//        $users = User::select(
//            DB::raw("id, name, lat ,users.long,
//                   (3959 * acos(cos
//                  ( radians(  ?  ) ) *
//                  cos( radians( users.lat ) ) *
//                  cos( radians( users.long ) - radians(?) ) +
//                  sin( radians(  ?  ) ) *
//                  sin( radians( users.lat ) )
//                  )) AS distance"))
//            ->setBindings([$user->lat, $user->long, $user->lat])
//            ->whereNotIn('id', [$user->id])
//            ->orderBy("distance")->get();
////        return $users;
//
//        $a = User::select(DB::raw("name"))->from($users)->get();
//        return $a;


//        $user = User::select(DB::raw('id, name,(YEAR(CURRENT_DATE())-YEAR(users.dob))as age'))->get();
//        return $user;


//        $age = 15;
//        $users = User::select(
//            DB::raw("*, (YEAR(CURRENT_DATE())-YEAR(users.dob))as age,
//                           (3959 * acos(cos
//                          ( radians(  ?  ) ) *
//                          cos( radians( users.lat ) ) *
//                          cos( radians( users.long ) - radians(?) ) +
//                          sin( radians(  ?  ) ) *
//                          sin( radians( users.lat ) )
//                          )) AS distance"))
//            ->setBindings([10.121212, 106.232323, 10.121212])->where('age', '<', $age + 3)
//            ->orWhere('age', '>', $age - 3)
//            ->whereNotIn('id', [2])
//            ->orderBy("distance")->get();

//        $age = 25;
//        $users = User::select(
//            DB::raw("*,(YEAR(CURRENT_DATE())-YEAR(users.dob)) as age,
//                           (3959 * acos(cos
//                          ( radians(  ?  ) ) *
//                          cos( radians( users.lat ) ) *
//                          cos( radians( users.long ) - radians(?) ) +
//                          sin( radians(  ?  ) ) *
//                          sin( radians( users.lat ) )
//                          )) AS distance"))
//            ->setBindings([10.121212, 106.232323, 10.121212])
//            ->whereNotIn('id', [2])->whereRaw('(YEAR(CURRENT_DATE())-YEAR(users.dob)) < ?', 20)
//            ->orderBy("distance")->get();
//        return $users;


//        $users = User::select(
//            DB::raw("*,(YEAR(CURRENT_DATE())-YEAR(users.dob)) as age,
//                           (3959 * acos(cos
//                          ( radians(  ?  ) ) *
//                          cos( radians( users.lat ) ) *
//                          cos( radians( users.long ) - radians(?) ) +
//                          sin( radians(  ?  ) ) *
//                          sin( radians( users.lat ) )
//                          )) AS distance"))
//            ->setBindings([10.121212, 106.232323, 10.121212])
//            ->whereNotIn('id', [2])->whereRaw('gender = ? and ((YEAR(CURRENT_DATE())-YEAR(users.dob)) between ? and ?)', [0, 18 - 3, 18 + 3])
//            ->orderBy("distance")->get();
//        return $users;


        $posts = DB::table('posts')
            ->join('users', 'users.id', '=', 'posts.user_id')
            ->select('users.*', 'posts.article', 'posts.created_at as created_post', 'posts.updated_at as updated_post')
            ->where('user_id', 2)
            ->orderBy('created_at', 'DESC')->paginate(2);

        return $posts;
    }
}
