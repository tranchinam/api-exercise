<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Token;
use http\Exception;
use Illuminate\Support\Facades\Lang;
use App\Http\Controllers\Controller;

class ApiAuthenticate extends Controller
{
    public function __construct()
    {
        $this->apiErrorCodes = Lang::get('apiErrorCode');
    }


    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            GLOBAL $err_mess;
            $token = $request->headers->get('Authorization');


//            if ($token && $token_db->expire_at >= now()) {
//                return $next($request);
//            } else if ($token && $token_db->expire_at <= now()) {
//                $err_mess = 'expired';
//            } else {
//                $err_mess = 'token_error';
//            }


            $err_mess = 'token_error';

            if ($token != null) {
                $token_db = Token::where('token', $token)->first();
                if ($token_db && $token_db->expire_at <= now())
                    $err_mess = 'expired';
                else return $next($request);
            }

            $response = array(
                "error" => true,
                "data" => null,
                "errors" => array([
                    'errorCode' => $this->apiErrorCodes['ApiErrorCodes'][$err_mess],
                    'errorMessage' => $this->apiErrorCodes[$err_mess]
                ])
            );
            return response()->json($response, 400, array());
        } catch
        (Exception $ex) {
            return $this->errorInternalError();
        }
    }
}
