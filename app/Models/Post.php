<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table='Posts';

    protected $fillable = [
        'user_id','article',
    ];


    public function user()
    {
        return $this->belongsto('App\Models\User','user_id');
    }
}
