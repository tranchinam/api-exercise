<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Relationship extends Model
{

    protected $fillable = [
        'user_id','friend_id',
    ];


    public function user()
    {
        return $this->belongsto('App\Models\User','user_id');
    }

    public function friend()
    {
        return $this->belongsto('App\Models\User','friend_id');
    }

}
