<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestFriend extends Model
{

    protected $table='Requests';

    protected $fillable = [
        'from_id','to_id', 'messages',
    ];


    public function fromUser()
    {
        return $this->belongsto('App\Models\User','from_id');
    }

    public function toUser()
    {
        return $this->belongsto('App\Models\User','to_id');
    }

}
