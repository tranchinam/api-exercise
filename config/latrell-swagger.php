<?php
//return array(
//	'enable' => env('SWAGGER_ENABLE', null),
//
//	'middleware' => 'web',
//	'prefix' => 'api-docs',
//
//	'paths' => [
//		app_path(),
//		base_path('routes')
//	],
//	'exclude' => null,
//
//	'title' => 'Swagger UI'
//);

/**
 * @SWG\Swagger(
 *   basePath="/api",
 *   @SWG\Info(
 *     title="MyApp API",
 *     version="0.2"
 *   )
 * )
 */

return array(
    'enable' => true,

    'prefix' => 'api-docs',

    'paths' => base_path('app'),
    'output' => storage_path('laravel/public'),
    'exclude' => null,
    'default-base-path' => null,
    'default-api-version' => null,
    'default-swagger-version' => null,
    'api-doc-template' => null,
    'suffix' => '.{format}',

    'title' => 'Swagger UI'
);