<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {

            $table->unsignedInteger('from_id');
            $table->foreign('from_id')->references('id')->on('users');

            $table->unsignedInteger('to_id');
            $table->foreign('to_id')->references('id')->on('users');

            $table->longText('messages');

            $table->timestamps();

        });
        DB::statement('ALTER TABLE  `requests` ADD PRIMARY KEY ( `from_id` ,  `to_id` )');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
