<?php

use Illuminate\Database\Seeder;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\DB;

class PasswordResetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $limit = 5;

        $now = now();
        $hours = 2;
        $expire = date_add($now, date_interval_create_from_date_string($hours. 'hours'));

        for ($i = 0; $i < $limit; $i++) {
            PasswordReset::create([
                'user_id' => rand(1,5),
                'code' => str_random(128),
                'expire_at' => $expire,
            ]);
//            DB::table('tokens')->insert([
//                'user_id' => rand(1,5),
//                'auth_token' => str_random(128),
//                'expire_at' => $expire,
//            ]);

        }
    }
}
