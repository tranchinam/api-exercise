<?php

use Illuminate\Database\Seeder;
use App\Models\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $faker = Faker\Factory::create('en_US');

        $limit = 20;

        for ($i = 0; $i < $limit; $i++) {
            Post::create([
                'user_id' => rand(1,10),
                'article' => $faker->sentence,
            ]);
        }
    }
}
