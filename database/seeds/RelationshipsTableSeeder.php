<?php

use Illuminate\Database\Seeder;
use App\Models\Relationship;

class RelationshipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $limit = 7;

        for ($i = 0; $i < $limit; $i++) {
            $user_id = rand(1, 10);
            $friend_id = rand(1, 10);
            while ($user_id == $friend_id) {
                $friend_id = rand(1, 10);
            }
            Relationship::create([
                'user_id' => $user_id,
                'friend_id' => $friend_id,
            ]);
            Relationship::create([
                'user_id' => $friend_id,
                'friend_id' => $user_id,
            ]);
        }
    }
}
