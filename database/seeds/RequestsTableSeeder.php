<?php

use Illuminate\Database\Seeder;
use App\Models\RequestFriend;

class RequestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');

        $limit = 4;

        for ($i = 0; $i < $limit; $i++) {
            $from_id = rand(1, 10);
            $to_id = rand(1, 10);
            while ($from_id == $to_id) {
                $to_id = rand(1, 10);
            }
            RequestFriend::create([
                'from_id' => $from_id,
                'to_id' => $to_id,
                'messages' => $faker->sentence,
            ]);
        }
    }
}
