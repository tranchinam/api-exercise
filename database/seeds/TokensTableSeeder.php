<?php

use Illuminate\Database\Seeder;
use App\Models\Token;
use Illuminate\Support\Facades\DB;

class TokensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $limit = 7;

        $now = now();
        $days = 2;
        $expire = date_add($now, date_interval_create_from_date_string($days. 'days'));

        for ($i = 0; $i < $limit; $i++) {
//            Token::create([
//                'user_id' => rand(1,5),
//                'token' => $faker->sentence,
//                'expire' => $expire
//            ]);
            DB::table('tokens')->insert([
                'user_id' => rand(1,5),
                'token' => str_random(128),
                'expire_at' => $expire,
            ]);

        }
    }
}
