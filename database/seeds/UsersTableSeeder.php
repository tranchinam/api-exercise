<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('en_US');

        $limit = 10;
        $dob = $faker->dateTime('now');
//        $dob => date_format($dob, "d-m-Y h-i-s");

        for ($i = 0; $i < $limit; $i++) {
            User::create([
                'email' => $faker->unique()->email,
                'password' => bcrypt('123456'),
                'name' => $faker->name(50),
                'dob' => date_format($dob, "Y-m-d"),
                'gender' => $faker->numberBetween(0, 1),
                'lat' => random_int(10000000, 15000000) / 1000000,
                'long' => random_int(100000000, 107000000) / 1000000,
                'zipcode' => '+'.rand(10,200),
                'phone' => random_int(1000000000, 1111111111),
                'job' => $faker->domainName,
            ]);
        }
    }
}