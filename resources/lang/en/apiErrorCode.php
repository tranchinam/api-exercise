<?php
$APICode = array(

    //create user begin
    'no_user' => 'Cant find user',
    'token_error' => 'Unauthorized',
    'expired' => 'Request Timeout: Your login session was expired',
    'no_login' => 'You aren\'t login yet!',
    'no_post_of_user' => 'User has not this post',

    'wrong_account' => 'Password is not right',
    'email_no_exist' => 'This email does not exist',
    'phone_no_exist' => 'This Phone number does not exist',
    'unavailable_page' => 'Page Not Found',
    'related' => 'This person already has been a friend',
    'not_related' => 'This person not a friend of you',
    'not_request' => 'You don\'t have any request friend from this person',


    'name_required' => 'Name is required',
    'name_string' => 'Name must be a string',
    'name_min' => 'Name is at least 3',
    'name_max' => 'Name may not be greater than 100',

    'email_required' => 'Email is required',
    'email_email' => 'Email must be a valid email',
    'email_regex' => 'Email format flow at least: xx@xx.com',
    'email_required_without' => 'Email is required if You don\'t have Phone number',

    'password_required' => 'Password is required',
    'password_string' => 'Password may not be only include number',
    'password_max' => 'Password may not be greater than 50',
    'password_confirmed' => 'Password does not match with Your Confirm Password',

    'confirm_password_required' => 'Confirm Password is required',
    'confirm_password_confirmed' => 'Confirm Password does not match with Your Password',
    'password_confirmation_required_with' => 'Confirm Password does not match with Your Password required',
    'password_confirmation_same' => 'Confirm Password does not match with Your Password ',

    'dob_required' => 'Date of birth is required',
    'dob_regex' => 'Date of birth format is: dd/mm/yyyy',

    'lat_required' => 'Latitude is required',
    'lat_regex' => 'Latitude format is: xxx.xxxxxx or xx.xxxxxx',

    'long_required' => 'Longitude is required',
    'long_regex' => 'Longitude format is: xxx.xxxxxx or xx.xxxxxx',


    'zipcode_required' => 'Zip code is required',
    'zipcode_regex' => 'Zip code is not valid Example: +84, +125',
    'phone_required' => 'Phone number is required',
    'phone_regex' => 'Phone number is a number include 10 or 11 letter: 016536426541',
    'phone_required_without' => 'Phone number is required if You don\'t have Email',
    'phone_regex_login' => 'Phone number inclde zipcode and phone, ex: +8494203213212',
    'phone_numeric' => 'Phone number include + and number',
    'job_string' => 'Job must be a string',

);
$apiErrorCodes = array(
    //create user begin
    'no_user' => 1000,
    'token_error' => 2000,
    'no_login' => 1500,

    'no_post_of_user' => 3404,
    'expired' => 1408,

    'wrong_account' => 1404,
    'email_no_exist' => 2404,
    'phone_no_exist' => 9404,
    'unavailable_page' => 5404,
    'related' => 5403,
    'not_related' => 6403,
    'not_request' => 7403,

    'name_required' => 5001,
    'name_string' => 5002,
    'name_min' => 5004,
    'name_max' => 5005,

    'email_required' => 5011,
    'email_email' => 5016,
    'email_regex' => 5017,
    'email_required_without' => 50111,

    'password_required' => 5021,
    'password_string' => 5022,
    'password_max' => 5025,
    'password_confirmed' => 5028,

    'confirm_password_required' => 5031,
    'confirm_password_confirmed' => 5038,
    'password_confirmation_required_with' => 50391,
    'password_confirmation_same' => 5039,

    'dob_required' => 5051,
    'dob_regex' => 5057,

    'lat_required' => 5061,
    'lat_regex' => 5067,

    'long_required' => 5071,
    'long_regex' => 5077,

    'zipcode_required' => 5081,
    'zipcode_regex' => 5087,

    'phone_required' => 5091,
    'phone_regex' => 5097,
    'phone_required_without' => 50911,
    'phone_regex_login' => 50977,
    'phone_numeric' => 509777,

    'job_string' => 5102,


);
$APICode['ApiErrorCodes'] = $apiErrorCodes;
$apiErrorCodesFlip = array_flip($apiErrorCodes);
$APICode['ApiErrorCodesFlip'] = $apiErrorCodesFlip;

return $APICode;