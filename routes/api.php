<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});





Route::prefix('auth')
    ->group(function () {
        Route::post('login', 'Api\AuthController@login');
        Route::post('register', 'Api\AuthController@register');
        Route::get('requestresetpass', 'Api\AuthController@resetPassword');
//        Route::get('/resetpass/{code}', function ($code) {
//            return view('reset-pass');
//        });
        Route::get('/resetpass/{code}', function($code)
        {
            return View::make('reset-pass',['code'=>$code]);
        });
//        Route::view('/resetpass/{code}', 'reset-pass', ['code' => ]);
        Route::post('/updatepassword','Api\AuthController@updatePassword');
    });


Route::middleware(['api.auth'])
    ->group(function () {
        Route::prefix('auth')
            ->group(function () {
                Route::post('logout', 'Api\AuthController@logout');
            });

        //Users
        Route::get('admin/users', 'Api\UserController@index'); //Admin
        Route::get('users', 'Api\UserController@showUser');
        Route::put('users', 'Api\UserController@updateUser');
        Route::delete('admin/users/{id}', 'Api\UserController@deleteUser'); //Admin

        //Posts
        Route::get('users/posts', 'Api\UserController@getPosts');
        Route::post('users/posts', 'Api\UserController@addPost');
        Route::put('users/posts/{post_id}', 'Api\UserController@updPost');
        Route::delete('users/posts/{post_id}', 'Api\UserController@deletePost');

        //Relationships
        Route::post('users/addfriend/{id}', 'Api\UserController@addFriend');
        Route::get('users/{id}/posts', 'Api\UserController@getPostsOfFriend');
        Route::get('users/newfeeds', 'Api\UserController@getNewFeeds');
        Route::get('users/findfriends', 'Api\UserController@findFriends');
        Route::get('users/sendrequest/{id}', 'Api\UserController@sendRequestFriend');
        Route::get('users/friendrequests', 'Api\UserController@friendRequests');
    });


Route::get('test/{id}', 'Api\UserController@test');

//Route::middleware('api.auth')->get('/users', 'Api\UserController@index');


Route::get('/', function () {
    return 'Test routes';
});





