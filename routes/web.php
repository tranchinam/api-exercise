<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/hello', function () {
    return 'test hello';
});


//Route::get('/test-database', 'UserController@index');


Route::get('/customers', function () {
    $faker = Faker\Factory::create();
    $limit = 5;
    $customers = [];
    for ($i = 0; $i < $limit; $i++) {
        $customers[$i] = [
            'Name' => $faker->name,
            'Email' => $faker->unique()->email,
            'Number' => $faker->phoneNumber,
            'Website' => $faker->domainName,
            'Age' => $faker->numberBetween(20, 100),
            'Address' => $faker->address
        ];
    }
    return response()->json($customers);
});

Route::get('/posts', function () {
    $posts = DB::table('posts')-> get()->where('user_id', '3');
    dd($posts);
});

Route::get('/posts/{id}', 'Api\UserController@getPosts');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
